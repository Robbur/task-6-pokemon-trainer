import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { TrainerPageComponent } from './pages/trainer-page/trainer-page.component';
import { PokemonDetailComponent } from './pages/pokemon-detail/pokemon-detail.component';
import { PokemonCatalogueComponent } from './pages/pokemon-catalogue/pokemon-catalogue.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { AuthGuard } from './guard/auth-guard';

const routes: Routes = [
  {
    path: 'login',
    component: LandingPageComponent,
  },
  {
    path: 'trainer',
    component: TrainerPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'details/:name',
    component: PokemonDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'catalogue',
    component: PokemonCatalogueComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
