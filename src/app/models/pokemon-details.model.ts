import { Sprites } from "./sprites.model";

export interface PokemonDetails {
    id?: number;
    name: string;
    url: string;
    abilities: any[];
    base_experience?: number;
    forms?: any;
    moves?: any[];
    sprites?: Sprites;
    stats?: any[];
    height?: number;
    weight?: number;
    types?: any[];
}