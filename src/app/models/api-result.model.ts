import { Results } from './results.model';

export interface APIResult {
    count: number;
    next: string;
    results: Results[];
}