import { PokemonDetails } from "./pokemon-details.model";

export interface Results {
    name: string;
    url: string;
    id?: string;
    details?: PokemonDetails;
    description?: string;
}