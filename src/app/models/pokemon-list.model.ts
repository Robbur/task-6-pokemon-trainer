import { PokemonDetails } from './pokemon-details.model';

export interface PokemonList {
    name: string;
    details: PokemonDetails;
}