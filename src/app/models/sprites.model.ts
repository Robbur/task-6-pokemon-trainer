import { Other } from './other.model';
export interface Sprites {
    other: Other;
    front_default: string;
}