import { Component, OnInit } from '@angular/core';
import { getTrainer, setTrainer } from '../../utils/storage.js';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (getTrainer('trainer')) {
      this.router.navigate(['/catalogue']);
    }
  }

}
