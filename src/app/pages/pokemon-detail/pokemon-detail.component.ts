import { Component, OnInit } from '@angular/core';
import { addPokemonToCollection, getCollectedPokemon } from '../../utils/storage.js';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api/api.service';
import { PokemonDetails } from '../../models/pokemon-details.model';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {

  public loadingPokemons: boolean;
  private readonly pokemonName: String = "";
  public pokemonDetails: PokemonDetails;
  public hasPokemon: boolean;
  public pokemonList: [] = getCollectedPokemon('pokemonList') || [];

  constructor(private router: ActivatedRoute, private api: ApiService) {
    this.pokemonName = String(this.router.snapshot.params.name);
  }

  ngOnInit(): void {
    this.loadingPokemons = false;
    this.getPokemonDetails();
    this.checkIfCaught();
  }

  get hasThisPokemon(): boolean {
    return this.hasPokemon;
  }

  // Check if pokemon is already caught
  checkIfCaught(): void {
    let pokemonList = getCollectedPokemon('pokemonList') || [];
    this.hasPokemon = pokemonList.find(p => p.name === this.pokemonName);
  }

  // Get pokemon details
  getPokemonDetails(): void {
    this.loadingPokemons = true;
    this.api
      .getPokemonDetails(this.pokemonName)
      .subscribe((details: PokemonDetails) => {
        this.pokemonDetails = details;
        this.loadingPokemons = false;
      });
  }

  // Catch pokemon
  onCatchClicked(): void {
    let pokemonList = getCollectedPokemon('pokemonList') || [];
    this.hasPokemon = pokemonList.find(p => p.name === this.pokemonName);
    if (this.hasPokemon) {
      alert("You already have this pokemon");
    } else {
      pokemonList.push({ name: this.pokemonName, details: this.pokemonDetails });
      addPokemonToCollection('pokemonList', pokemonList);
      this.hasPokemon = true;
      alert("You caught " + this.pokemonName);
    }
  }

  // Release pokemon
  onReleaseClicked() {
    if (confirm('Are you sure?')) {
      let pokemonList = getCollectedPokemon('pokemonList') || [];
      pokemonList = pokemonList.filter(item => item.name !== this.pokemonName);
      addPokemonToCollection('pokemonList', pokemonList);
      this.hasPokemon = false;
      alert("You released " + this.pokemonName);
    }
  }
}
