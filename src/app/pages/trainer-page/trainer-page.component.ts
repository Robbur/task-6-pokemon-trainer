import { Component, OnInit } from '@angular/core';
import { getCollectedPokemon } from '../../utils/storage.js';
import { Router } from '@angular/router';
import { PokemonList } from '../../models/pokemon-list.model';
import { removePokemonFromCollection, removeTrainer } from '../../utils/storage.js';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css']
})
export class TrainerPageComponent implements OnInit {

  public list: PokemonList;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.list = getCollectedPokemon('pokemonList');
  }

  handlePokemonClicked(name): void {
    this.router.navigateByUrl(`/details/${name}`);
  }

  handleLogoutClicked() {
    if (confirm('Are you sure?')) {
      // Removes localStorage
      removePokemonFromCollection('pokemonList');
      removeTrainer('trainer');
      this.router.navigateByUrl('/login');
    } else {
      return;
    }
  }

}
