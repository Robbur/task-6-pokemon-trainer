import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { APIResult } from '../../models/api-result.model';
import { PokemonDetails } from '../../models/pokemon-details.model';
import { Results } from '../../models/results.model';
import { ApiService } from '../../services/api/api.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css']
})
export class PokemonCatalogueComponent implements OnInit {

  @Output() pokemonList = new EventEmitter();

  public loadingPokemons: boolean;
  pokemons: APIResult;

  constructor(private router: Router, private api: ApiService) { }

  ngOnInit(): void {
    this.loadingPokemons = false;
    this.getAllPokemons();
  }

  // Retrieve all pokemon
  getAllPokemons(): void {
    this.api.getOriginalPokemons().subscribe((res: APIResult) => {
      this.pokemons = res;

      if (this.pokemons.results && this.pokemons.results.length) {
        this.pokemons.results.forEach(pokemon => {
          pokemon.id = pokemon.url.split('/')[
            pokemon.url.split('/').length - 2
          ];
          this.getPokemonDetails(pokemon);
        })
      }
    })
  }

  // Get pokemon details
  getPokemonDetails(pokemon: Results): void {
    this.api
      .getPokemonDetails(pokemon.name)
      .subscribe((details: PokemonDetails) => {
        pokemon.details = details;
        if (pokemon.id === '151') {
          this.loadingPokemons = true;
          this.pokemonList.emit(this.pokemons.results);
        }
      });
  }

  // Navigate to detail page for pokemon clicked
  handlePokemonClicked(name): void {
    this.router.navigateByUrl(`/details/${name}`);
  }
}
