export const setTrainer = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

export const getTrainer = key => {
    const storedValue = localStorage.getItem(key);
    if (storedValue) {
        return JSON.parse(storedValue);
    }
    return false;
}

export const removeTrainer = key => {
    localStorage.removeItem(key);
}

export const addPokemonToCollection = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

export const getCollectedPokemon = key => {
    const storedValue = localStorage.getItem(key);
    if (storedValue) {
        return JSON.parse(storedValue);
    }
    return false;
}

export const removePokemonFromCollection = key => {
    localStorage.removeItem(key);
}
