import { Component, OnInit } from '@angular/core';
import { getTrainer, setTrainer, addPokemonToCollection } from '../../utils/storage.js';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  trainer = {
    trainername: ''
  }
  public inputError: boolean = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  // Handle login button clicked
  onLoginClicked() {
    if (getTrainer('trainer')) {
      return;
    } else if (this.trainer.trainername === '' || this.trainer.trainername.length == 0) {
      this.inputError = true;
    } else {
      setTrainer('trainer', this.trainer.trainername);
      this.inputError = false;
      addPokemonToCollection('pokemonList', []);
      this.router.navigate(['/catalogue']);
    }
  }
}
