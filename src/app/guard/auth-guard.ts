import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { getTrainer } from '../utils/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {

  constructor(private router: Router) { }

  canActivate() {
    if (getTrainer('trainer')) {
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
