import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Results } from '../../models/results.model';

@Component({
  selector: 'app-pokemon-item',
  templateUrl: './pokemon-item.component.html',
  styleUrls: ['./pokemon-item.component.css']
})
export class PokemonItemComponent implements OnInit {

  @Input() thisPokemon: Results;
  @Output() pokemonClicked: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onPokemonClicked() {
    this.pokemonClicked.emit(this.thisPokemon.name);
  }

}
